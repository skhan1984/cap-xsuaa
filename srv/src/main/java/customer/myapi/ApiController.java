package customer.myapi;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/v1")
public class ApiController {
    @GetMapping(path = "/hello")
    public String sayHello(){
        return "Hello World";
    }
    
}
 